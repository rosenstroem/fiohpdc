
## fiohpdc R package

This package allows users to map their working hour time series data onto the eight clusters found from work shift-scheduling data from the Southwestern hospital district of Finland in the following work:
*Patterns of working hour characteristics and risk of sickness absence in shift working hospital employees: a data-mining cohort study* 
by Rosenström T, Härmä M, Kivimäki M, Ervasti J, Virtanen M, Hakola T, Koskinen A, Ropponen A, published at 
*Scandinavian Journal of Work, Environment and Health* 2021;47(5):395–403. https://doi.org/10.5271/sjweh.3957

The work was conducted at Finnish Institute of Occupational Health (FIOH) using Permutation Distribution Clustering (PDC). Hence, the package name "fiohpdc". The package depends on the `pdc` R package, which `fiohpdc` supplements with additional scripts, and most importantly, with the mapped employee-specific codebooks that allow mapping new data onto the previously detected clusters using a nearest neighbor algorithm.

The package transforms new 3-dimensional time series data into codebooks and derives their symmetric alpha divergences from the codebooks based on FIOH data. It then assigns the new data with cluster labels corresponding to their nearest neighbor in the FIOH data. This allows other research to map their data to clusters close to ours in content, or to study how well their cluster solutions replicate ours.

---

## Installation

To install, you need an R software version 3.5.1, or higher. It is freely available on https://r-project.org

Download the repository ("Downloads" on the left-hand panel). On your local machine, extract the files and open R on the folder where you extracted the files. In R command line, type:

```r
install.packages("fiohpdc_0.0.0.9000.tar.gz", repos = NULL, type = "source") 
```

Then, hit enter and you are set to go. You may need to restart R session after fiohpdc installation, if R throws an error about lazy-load database upon use (a persisting base R bug).

---

## Example

Users may have their own working hour datasets, but we illustrate the package use via simulated working hour data. Simulation studies could also be used to understand the algorithm and the empiric codebooks (our data) behind it.
You try the following, for example.

Let's analyze two simulated employees, one with highly regular work shifts over a 3-year period and another with very irregular working hours. First, initialize a 3-D data matrix in the style of the pdc R package and pad
it with missing values (`NA`). Dimensions of the matrix correspond to work shifts (time series length), employees (2 in the example), and work hour characteristics (3 altogether; work shift length, recovery length, and 
starting time of the shift in hours).

```r
X <- array(NA, dim = c(780, 2, 3))
```

The length of the time series was decided based on the first simulated employee having 5 shifts per week for 3 years, each having 52 weeks (i.e., `5*3*52`, or 780 shifts, altogether). You could define arbitrarily long time series,
but note that too short series will lead to ill-estimated permutation codebooks. Suppose our first employee on average works 8-hour shifts from monday to thursday and 6 hours in fridays (we omit vacations for simplicity). The 
remaining time of the week she rests, or is off work. Nevertheless, some minor variance exists in her timecard entries due to variable times she spends traveling, taking kids to day care, etc. We simulate her work hour time
series followingly:

```r
set.seed(2601) # a random seed you may change (use this seed to reproduce our exact result)
X[,1,1] <- rnorm(780, mean = rep(c(8,8,8,8,6),3*52), sd = 0.2) # work shift lengths with Gaussian noise
X[,1,2] <- rep(c(24,24,24,24,24*3), 3*52) - X[,1,1]            # recovery times left after the work shifts
X[,1,3] <- rnorm(780, mean = rep(8,780), sd = 0.1)             # shift starting times
```

Another employee works on very irregularly timed work shifts, on average only four 10-hour shifts a week. She is equally likely to start her work shift any time of day and night. Because she has 1 work shift less per week than her 
colleague, we fill in only the 624 first entries (3 years times 4 shifts per week times 52 weeks) and leave the value `NA` for the remaining entries:

```r
X[1:624,2,1] <- rnorm(624, mean = rep(10,780), sd = 1)         # work shift lengths for the 2nd employee
X[1:624,2,2] <- rnorm(624, mean = rep(14,780), sd = 1)         # recovery lengths
X[1:624,2,3] <- runif(624, min = 0, max = 24)                  # shift starting times
```

Having our data ready, using the fiohpdc package to assign it to the FIOH clusters is very easy. Simply call:
```r
library(fiohpdc)
fiohClusterSolve(X)
```

Alternatively, you can read the cluster labels into a variable without attaching the R package:
```r
mylabels <- fiohpdc::fiohClusterSolve(X)
```

Running the above lines, we found our two employees were assigned to clusters 1 and 5, respectively. In our paper, the cluster 1 was associated with regular working hours and a low risk of sickness absences, whereas the
cluster 5 was associated with more irregular working times and a high risk of sickness absences. Please, feel free to explore what combinations of working hour characteristics lead to which cluster memberhips and to examine their
associated risk profiles from our paper and other literature.